/*
GAME RULES:

- The game has 2 players, playing in rounds.
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score.
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn.
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score. After that,
it's the next player's turn.
- The first player to reach 20 points on GLOBAL score wins the game.
*/
/*****************************************************************************************************************/
// Declare variables
var score, roundScore, activePlayer, gamePlaying;

score = [0, 0];
roundScore = 0;
activePlayer = 0;
alert('Welcome to Pig Game\nClick \'New game\' to play!');
document.querySelector('.dice').style.display = 'none';

// Button New game
document.querySelector('.btn-new').addEventListener('click', init);

// Button Roll
document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gamePlaying) {
        var dice = Math.floor(Math.random() * 6) + 1;
        document.querySelector('.dice').src = 'dice-' + dice + '.png';
        if (dice !== 1) {
            roundScore += dice;
            document.getElementById('current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }
    } else {
        alert('Click \'New game\' to play!');
    }
});

// Button Hold
document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gamePlaying) {
        score[activePlayer] += roundScore;
        document.getElementById('score-' + activePlayer).textContent = score[activePlayer];
        roundScore = 0;
        if (score[activePlayer] >= 20) {
            alert('The Winner is Player ' + (activePlayer + 1));
            document.getElementById('name-' + activePlayer).textContent = 'WINNER!';
            document.querySelector('.player-0-panel').classList.remove('active');
            document.querySelector('.player-1-panel').classList.remove('active');
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            gamePlaying = false;
        } else {
            nextPlayer();
        }

    } else {
        alert('Click \'New game\' to play!');
    }
})

// Define function
function init() {
    score = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gamePlaying = true;

    document.getElementById('score-0').textContent = 0;
    document.getElementById('score-1').textContent = 0;
    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.add('active');
    document.querySelector('.dice').style.display = 'block';
}

function nextPlayer() {
    if (activePlayer === 0) {
        alert('Player 2\'s turn!');
        activePlayer = 1;
    } else {
        alert('Player 1\'s turn!');
        activePlayer = 0;
    }
    roundScore = 0;

    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
}